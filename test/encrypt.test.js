const { describe } = require('tape-plus')
const fs = require('fs')
const path = require('path')
const os = require('os')
const { pipeline } = require('stream')
const hyperdrive = require('hyperdrive')

const streamCipher = require('../encrypt')
const util = require('./helpers/util')

const testFilePath = path.join(path.resolve(__dirname), '../package.json')
const testFileContents = fs.readFileSync(testFilePath, 'utf8')

describe('encrypt', (context) => {
  let testStorage

  context.beforeEach(() => {
    testStorage = util.makeTmpDir()
  })

  context.afterEach(async () => {
    await util.rmTmpDir(testStorage)
  })

  context('encrypt file and decrypt file', async (assert, next) => {
    const key = streamCipher.key()
    const encryptStream = streamCipher(key)

    const encryptedFile = path.join(testStorage, 'encryptedFileStored')

    await new Promise((resolve, reject) => {
      pipeline(
        fs.createReadStream(testFilePath),
        encryptStream,
        fs.createWriteStream(encryptedFile),
        (err) => {
          if (err) return reject(err)
          resolve()
        }
      )
    })
      .catch(assert.error)

    const encryptedFileContents = fs.readFileSync(encryptedFile, 'utf8')

    assert.notEqual(encryptedFileContents, testFileContents, 'file successfully encrypted')

    // WIP - think it breaks on this next line
    const decryptStream = streamCipher(key)
    const source = fs.createReadStream(encryptedFile)
      .pipe(decryptStream)

    let decryptedContents = ''
    for await (const data of source) {
      decryptedContents += data.toString('utf8')
    }

    assert.equal(testFileContents, decryptedContents, 'file suscessfully decrypted')
    next()
  })

  context('put encrypted file in a hyperdrive', async (assert, next) => {
    const key = streamCipher.key()
    const encryptStream = streamCipher(key)
    const drive = hyperdrive(testStorage)

    const errorFromPipeline = await new Promise((resolve, reject) => {
      pipeline(
        fs.createReadStream(testFilePath),
        encryptStream,
        drive.createWriteStream('package.json'),
        (err) => {
          if (err) return reject(err)
          resolve()
        }
      )
    }).catch((err) => { return err })
    assert.error(errorFromPipeline, 'No error from pipeline')

    const decryptStream = drive.createReadStream('package.json')
      .pipe(streamCipher(key))

    let decryptedContents = ''
    for await (const data of decryptStream) {
      decryptedContents += data.toString('utf8')
    }
    assert.equal(testFileContents, decryptedContents, 'file successfully decrypted')
    next()
  })

  context('offset read', async (assert, next) => {
    const key = streamCipher.key()
    const encFilePath = path.join(
      os.tmpdir(),
      'artefact-store-' + path.parse(testFilePath).base + '.enc' // package.json
    )

    const pipelineError = await new Promise((resolve, reject) => {
      pipeline(
        fs.createReadStream(testFilePath),
        streamCipher(key),
        fs.createWriteStream(encFilePath),
        (err) => {
          if (err) return reject(err)
          resolve()
        }
      )
    }).catch((err) => { return err })
    assert.error(pipelineError, 'No error from encrypt pipeline')

    const decryptStream = fs.createReadStream(encFilePath, { start: 5 })
      .pipe(streamCipher(key, 5))

    let partialDecryptedContents = ''
    for await (const data of decryptStream) {
      partialDecryptedContents += data.toString('utf8')
    }

    assert.equal(partialDecryptedContents, testFileContents.slice(5), 'Portion of file successfully decrypted')
    next()
  })
})
