const { describe } = require('tape-plus')
const fs = require('fs')
const path = require('path')

const ArtefactStore = require('..')
const localUtil = require('./helpers/util')

describe('artefactStore.getFilesByDrive', (context) => {
  context('test getFilesByDrive for multiple stores', async (assert, next) => {
    const testStorageAlice = localUtil.makeTmpDir()
    const testStorageBob = localUtil.makeTmpDir()

    const alice = new ArtefactStore(testStorageAlice)
    const bob = new ArtefactStore(testStorageBob)
    await Promise.all([
      alice.ready(),
      bob.ready()
    ])

    const addFileToAlice = AddFile(alice)

    // bob registers alice
    await bob.addDrive(alice.driveAddress)

    const bobSizeAfterRegister = await localUtil.totalSize(testStorageBob)
    const aliceSizeAfterRegister = await localUtil.totalSize(testStorageAlice)

    // alice adds two new files
    await addFileToAlice('README.md')
    await addFileToAlice('TESTFILE.md')

    // she can see them
    assert.deepEqual(
      await alice.getFilesByDrive(),
      ['README.md', 'TESTFILE.md'],
      'alice sees the two files she added'
    )

    // bob still doesnt have any files
    assert.deepEqual(
      await bob.getFilesByDrive(),
      [],
      'bob can see none of his own files'
    )

    // get alices size with two files
    const aliceSizeWithFiles = await localUtil.totalSize(testStorageAlice)

    // get bobs size after he registers alices drive
    const bobSizeWithAlicesFiles = await localUtil.totalSize(testStorageBob)

    assert.true(bobSizeWithAlicesFiles > bobSizeAfterRegister, 'bobs drive got bigger')
    assert.true(aliceSizeWithFiles > bobSizeWithAlicesFiles, 'alices drive is still bigger then bobs')

    // see how much bobs size increased
    const bobSizeIncrease = bobSizeWithAlicesFiles - bobSizeAfterRegister
    const aliceSizeIncrease = aliceSizeWithFiles - aliceSizeAfterRegister

    // check how much bobs size increased vs size of file
    assert.true(aliceSizeIncrease > bobSizeIncrease, 'bobs size increase wasnt as big as alices')

    assert.deepEqual(
      await bob.getFilesByDrive('/', alice.driveAddress),
      ['README.md', 'TESTFILE.md'],
      'bob sees alices files'
    )

    // get the file sizes
    const readmeSize = await alice.fileSize('README.md', alice.driveAddress)
    const testfileSize = await alice.fileSize('TESTFILE.md', alice.driveAddress)

    assert.true(aliceSizeIncrease > (readmeSize + testfileSize), 'alices size increased by more then the total of file sizes')

    // check bob didnt get the files
    assert.true(bobSizeIncrease < (readmeSize + testfileSize), 'bobs size increased, but less then the total of file sizes')

    alice.close()
    bob.close()
    await localUtil.rmTmpDir(testStorageAlice)
    await localUtil.rmTmpDir(testStorageBob)
    next()
  })
})

function AddFile (store) {
  return function addFile (fileName) {
    const key = ArtefactStore.artefactEncryptionKey()
    const file = fs.createReadStream(path.join(path.resolve(__dirname), `./data/${fileName}`))
    const storeFile = store.createWriteStream(`./${fileName}`, key)
    file.pipe(storeFile)
    return store.fileReady(`./${fileName}`)
  }
}
