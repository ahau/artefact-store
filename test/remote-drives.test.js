const { describe } = require('tape-plus')

const ArtefactStore = require('..')
const localUtil = require('./helpers/util')

describe('artefactStore.remoteDrives', (context) => {
  context('test getting remote drives for a store', async (assert, next) => {
    const testStorageAlice = localUtil.makeTmpDir()
    const testStorageBob = localUtil.makeTmpDir()

    // bob registers alic
    const alice = new ArtefactStore(testStorageAlice)
    const bob = new ArtefactStore(testStorageBob)
    await Promise.all([
      alice.ready(),
      bob.ready()
    ])

    await bob.addDrive(alice.driveAddress)

    assert.deepEqual(bob.remoteDrives.map(drive => drive.key), [alice.driveAddress], 'the key on the drive given from bobs remote drives matches alices driveAddress')

    assert.notEqual(alice.driveAddress, bob.driveAddress, 'alice and bob have different driveAddress')

    alice.close()
    bob.close()

    await Promise.all([
      localUtil.rmTmpDir(testStorageAlice),
      localUtil.rmTmpDir(testStorageBob)
    ])

    next()
  })
})
