const path = require('node:path')
const test = require('tape')
const DHT = require('@hyperswarm/dht')

const ArtefactStore = require('..')
const localUtil = require('./helpers/util')

async function dhtTest({ t, bootstrap, replicateShouldWork }) {
  const testStorageAlice = localUtil.makeTmpDir()
  const testStorageBob = localUtil.makeTmpDir()

  const opts = {
    ...(bootstrap ? { bootstrap } : null)
  }

  const alice = new ArtefactStore(testStorageAlice, opts)
  const bob = new ArtefactStore(testStorageBob, opts)
  await Promise.all([
    alice.ready(),
    bob.ready()
  ])

  const inputFile = path.join(path.resolve(__dirname), '../README.md')

  await alice.addFile(inputFile)

  await bob.addDrive(alice.driveAddress)

  const aliceFiles = await alice.getFilesByDrive('/', alice.driveAddress)
  const bobFiles = await bob.getFilesByDrive('/', alice.driveAddress)

  if (replicateShouldWork) {
    t.deepEqual(bobFiles, aliceFiles, "bob got alice's files")
  } else {
    t.deepEqual(aliceFiles, ["README.md"], "alice got her files")
    t.deepEqual(bobFiles, [], "bob doesn't have alice's files")
  }

  await Promise.all([
    alice.close(),
    bob.close()
  ])

  await Promise.all([
    localUtil.rmTmpDir(testStorageAlice),
    localUtil.rmTmpDir(testStorageBob)
  ])
}

test('test sharing file with friend using a local dht bootstrap node', async (t) => {
  // This test should work with or without mdns enabled. To disable mdns, see comment below.

  let dht = null
  
  dht = DHT({ bootstrap: [], ephemeral: false })

  dht.listen(9872)

  await new Promise(resolve => dht.on('ready', resolve))

  await dhtTest({
    t,
    bootstrap: [
      '127.0.0.1:9872',
    ],
    replicateShouldWork: true
  })

  dht.destroy()
})

test('test sharing file with friend using our online self hosted dht bootstrap node', async (t) => {
  // This test should work with or without mdns enabled. To disable mdns, see comment below.

  // This should already use our own bootstrap node, it's the new default in this module
  await dhtTest({
    t,
    replicateShouldWork: true
  })

  // The entire code for the bootstrap node looks like:
  //const dht = require('@hyperswarm/dht') // v4.0.1

  //const node = dht({
  //  ephemeral: false,
  //  bootstrap: []
  //})

  //const PORT = 49737
  //node.listen(PORT, function () {
  //  const address = node.address()
  //  console.log(address)
  //})
})

test.skip("test failing to share a file with a friend because we've disabled mdns and the default bootstrap nodes are down", async t => {
  // To have this test pass you need to manually disable mdns, which is why we skip this test by default
  // Go into node_modules/multicast-dns/index.js and make the code at line 76 look like
  //that.send = function (value, rinfo, cb) {
  //  console.log('mdns disabled!')
  //  return cb?.()

  await dhtTest({
    t,
    // These were the old hyper dht bootstrap nodes that have been taken down
    bootstrap: [
      'bootstrap1.hyperdht.org:49737',
      'bootstrap2.hyperdht.org:49737',
      'bootstrap3.hyperdht.org:49737'
    ],
    replicateShouldWork: false
   })
})