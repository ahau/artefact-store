const { describe } = require('tape-plus')
const fs = require('fs')
const path = require('path')
const { promisify } = require('util')

const ArtefactStore = require('..')
const localUtil = require('./helpers/util')
const stat = promisify(fs.stat)

describe('artefactStore.fileStats', (context) => {
  context('find the stats of a given file on a drive', async (assert, next) => {
    const testStorageAlice = localUtil.makeTmpDir()
    const testStorageBob = localUtil.makeTmpDir()
    const key = ArtefactStore.artefactEncryptionKey()

    const file1Path = path.join(path.resolve(__dirname), './data/README.md')

    const alice = new ArtefactStore(testStorageAlice)
    const bob = new ArtefactStore(testStorageBob)
    await Promise.all([
      alice.ready(),
      bob.ready()
    ])

    const lowerBound = new Date()
    const file1 = fs.createReadStream(file1Path)
    const bobFile1 = bob.createWriteStream('./README.md', key)
    file1.pipe(bobFile1)
    await bob.fileReady('./README.md')
    const upperBound = new Date()

    const originalFileStats = await stat(file1Path)

    const driveFileStats = await alice.fileStats('README.md', bob.driveAddress)

    assert.true(driveFileStats.ctime >= lowerBound, 'stat.ctime >= lowerBound')
    assert.true(driveFileStats.ctime <= upperBound, 'stat.ctime <= upperBound')

    assert.equal(driveFileStats.size, originalFileStats.size, 'alice correctly determines the size of the file on bobs drive')

    // fs.stat and artefactStore.fileStats do not return the exact same values

    alice.close()
    bob.close()
    await Promise.all([
      localUtil.rmTmpDir(testStorageAlice),
      localUtil.rmTmpDir(testStorageBob)
    ])
    next()
  })

  context('does not fetch files it does not have', async (assert, next) => {
    const testStorageAlice = localUtil.makeTmpDir()
    const testStorageBob = localUtil.makeTmpDir()
    const key = ArtefactStore.artefactEncryptionKey()

    const alice = new ArtefactStore(testStorageAlice)
    const bob = new ArtefactStore(testStorageBob)
    await Promise.all([
      alice.ready(),
      bob.ready()
    ])

    // Bob writes files to his drive
    const file1 = fs.createReadStream(path.resolve(__dirname, './data/README.md'))
    const bobFile1 = bob.createWriteStream('./README.md', key)
    file1.pipe(bobFile1)
    await bob.fileReady('./README.md')

    await new Promise(resolve => setTimeout(resolve, 1000)) // wait to see if replication stays sparse

    try {
      const stats = await alice.fileStats('./README.md', bob.driveAddress) // alice asking about her local copy of bob's drive
      assert.true(stats.size > 0, 'successfully fileStats someone elses file')
    } catch (err) {
      console.error(err)
    }

    const size = await alice.driveSize(bob.driveAddress) // alice's local, sparse copy of bob's drive
    assert.equal(size, 0, 'alice asking calling fileStats on bobs drive did not fetch anything')

    alice.close()
    bob.close()

    await Promise.all([
      localUtil.rmTmpDir(testStorageAlice),
      localUtil.rmTmpDir(testStorageBob)
    ])
    next()
  })
})
