const sodium = require('sodium-native')
const { Transform } = require('stream')
const OFFSET_CHUNK_SIZE = 65535

// context is a nonce or extra information that we derive a nonce from e.g. the group key
// offset is the byte number of the location in the e.g. video/audio etc that the user 'jumps' to
module.exports = function (msgKey, context, offset) {
  if (typeof context === 'number' && !offset) {
    offset = context
    context = null
  }

  // If we are not given something to make a nonce from, derive from the key
  // This is only ok to do if this cipher is only ever used once
  context = context || msgKey

  // sodium.crypto_stream_NONCEBYTES = 24 bytes (constant)
  // bc we want a nonce of consistently 24 bytes, but hashing [in this case context] will always produce 32
  const nonce = genericHash(context).slice(0, sodium.crypto_stream_NONCEBYTES)

  const xorState = Buffer.alloc(sodium.crypto_stream_xor_STATEBYTES)
  sodium.crypto_stream_xor_init(xorState, nonce, msgKey)

  if (offset) {
    const offsetBuffer = Buffer.alloc(OFFSET_CHUNK_SIZE)
    let bytesStillToRead = offset
    while (bytesStillToRead) {
      const toRead = offsetBuffer.slice(0, Math.min(bytesStillToRead, OFFSET_CHUNK_SIZE))
      sodium.crypto_stream_xor_update(xorState, toRead, toRead)
      bytesStillToRead -= toRead.length
    }
  }

  return new Transform({
    transform (chunk, enc, callback) {
      sodium.crypto_stream_xor_update(xorState, chunk, chunk)
      callback(null, chunk)
    }
  }).on('close', () => {
    sodium.crypto_stream_xor_final(xorState)
  })
}

module.exports.key = function () {
  // allocate a buffer (can also do Buffer.alloc, but sodium is apparently 'extra secure')
  // at this point buf is then 32 x '0' bytes
  const buf = sodium.sodium_malloc(sodium.crypto_stream_KEYBYTES)

  // this takes the buffer as an argument, and fills it with randomness
  sodium.randombytes_buf(buf)

  // this returns the 32 bytes of randomness as a key
  return buf
}

function genericHash (msg) {
  const hash = sodium.sodium_malloc(sodium.crypto_generichash_KEYBYTES)
  sodium.crypto_generichash(hash, msg)
  return hash
}
