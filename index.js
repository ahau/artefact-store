const fs = require('fs')
const path = require('path')
const { promisify: p } = require('util')
const Hyperdrive = require('hyperdrive')
const Corestore = require('corestore')
const Networker = require('@corestore/networker')
const RAF = require('random-access-file')
const EventEmitter = require('events')
const Obz = require('obz')

const stat = p(fs.stat)
const log = require('debug')('artefact-store')

const streamCipher = require('./encrypt')
const { edToCurvePk, edToCurveSk, printKey, toString, toBuffer, isKey } = require('./util')

const DRIVE_ADDRESS_LENGTH = 32
const ENCRYPTION_KEY_LENGTH = 32 // TODO get these constants from sodium

module.exports = class ArtefactStore extends EventEmitter {
  constructor (storagePath, options = {}) {
    super()

    this.storagePath = storagePath
    const storage = typeof storagePath === 'string'
      ? p => RAF(path.join(storagePath, p))
      : storagePath

    this.store = new Corestore(storage)
    this.noiseKeyPair = null
    this.networker = null
    this.drives = {
      personal: null,
      remote: new DriveStore(),
      loading: new DriveLoading(),
      persistedThisSession: new Set()
    }

    this.isPataka = Boolean(options.pataka)
    this.isReady = false
    this.options = options
  }

  // Called after instantiating ArtefactStore, ensures user's own drive keys are correctly recorded.
  async ready () {
    if (this.isReady) return

    await this.store.ready()
    this.driveList = this.store.default({ valueEncoding: 'utf-8' })

    if (!this.isPataka) await this._setupMyDrive()

    this.networker = new Networker(this.store, Object.assign({
      keyPair: this.noiseKeyPair,
      announceLocalAddress: true,
      ephemeral: false,
      bootstrap: [
        '157.230.33.120:49737',
      ],
    }, this.options))
    this.networker.on('peer-add', (peer) => this.emit('peer-add', peer))
    this.networker.on('peer-remove', (peer) => this.emit('peer-remove', peer))

    if (!this.isPataka) {
      // Make ourself addressable by announcing our discovery key
      await this.networker.configure(this.discoveryKey, { announce: true, lookup: true })
      // await new Promise((resolve, reject) => {
      //   this.networker.once('opened', resolve)
      //   this.networker.configure(this.discoveryKey, { announce: true, lookup: true })
      // })
    }

    if (this.networker.swarm) {
      log('Hyperswarm is instantiated')
      this.networker.swarm.on('connection', (socket) => {
        log('Connection to peer', socket._peername ? socket._peername.address : '')
      })
      this.networker.swarm.on('disconnection', (socket) => {
        log('Disconnection from peer', socket._peername ? socket._peername.address : '')
      })
      // logEvents(this.networker.swarm)
    }
    // Reconnect to known drives

    const self = this
    for await (const driveAddress of this.driveList.createReadStream()) {
      if (this.drives.remote.has(driveAddress)) continue

      log('Loading previously connected drive ', printKey(driveAddress))
      await self._loadDrive(driveAddress)
    }

    this.isReady = true
  }

  async _setupMyDrive () {
    if (this.isPataka) throw new Error('_setupMyDrive not a valid pataka method')

    const self = this
    self.drives.personal = new Hyperdrive(this.store.namespace('myDrive'), { sparse: false })
    await new Promise((resolve, reject) => {
      self.drives.personal.on('ready', resolve)
    })
    log(`Own hyperdrive ready, driveAddress: ${printKey(this.driveAddress)} discovery: ${printKey(this.discoveryKey)}`)
    this.noiseKeyPair = {
      publicKey: edToCurvePk(this.driveAddress),
      secretKey: edToCurveSk(this.drives.personal.metadata.secretKey)
    }
    log('Using noise keypair ', printKey(this.noiseKeyPair.publicKey), printKey(this.noiseKeyPair.secretKey))
  }

  async getFilesByDrive (directoryPath = '', driveAddress) {
    // if we are not given a drive address, return files in our own drive
    if ((!driveAddress) && (!this.isPataka)) driveAddress = this.driveAddress
    if (!isKey(driveAddress, DRIVE_ADDRESS_LENGTH)) return Promise.reject(new Error('Badly formatted drive address'))
    const drive = await this._getDrive(driveAddress)

    const files = await new Promise((resolve, reject) => {
      drive.readdir(directoryPath, { recursive: true, noMount: true }, (err, files) => err ? reject(err) : resolve(files))
    })

    return files
  }

  async getConnectedPeersForDrive (driveAddress) {
    // If we are not given a drive address, return peers connected to our own drive
    if ((!driveAddress) && (!this.isPataka)) driveAddress = this.driveAddress
    if (!isKey(driveAddress, DRIVE_ADDRESS_LENGTH)) return Promise.reject(new Error('Badly formatted drive address'))
    const drive = await this._getDrive(driveAddress)
    return drive.peers
  }

  // TODO rename registerDrive
  async addDrive (driveAddress) {
    if (!isKey(driveAddress, DRIVE_ADDRESS_LENGTH)) return Promise.reject(new Error('Badly formatted drive address'))

    const drive = await this._getDrive(driveAddress)
    if (!drive.peers.length) await this._connect(drive)
    return drive
  }

  async _getDrive (driveAddress) {
    driveAddress = toBuffer(driveAddress)

    if ((!this.isPataka) && Buffer.compare(driveAddress, this.driveAddress) === 0) {
      return this.drives.personal
    }

    if (this.drives.remote.has(driveAddress)) {
      log('Grabbing already instantiated drive', printKey(driveAddress))

      const drive = this.drives.remote.get(driveAddress)
      await new Promise((resolve, reject) => drive.ready(resolve))
      if (!drive.peers.length) await this._connect(drive)

      return drive
    }

    this._persistDriveAddress(driveAddress)
      .catch(err => console.error(`failed to persist, ${driveAddress}`, err))

    return this._loadDrive(driveAddress)
  }

  async _persistDriveAddress (driveAddress) {
    driveAddress = driveAddress.toString('hex')
    if (this.drives.persistedThisSession.has(driveAddress)) return Promise.resolve()

    log('Instantiating drive for first time ', printKey(driveAddress))
    this.drives.persistedThisSession.add(driveAddress)

    const self = this
    return new Promise((resolve, reject) => {
      self.driveList.append(driveAddress, (err) => {
        if (err) {
          this.drives.persistedThisSession.delete(driveAddress)
          return reject(err)
        }

        resolve()
      })
    })
  }

  async _loadDrive (driveAddress) {
    if (this.drives.loading.has(driveAddress)) {
      return await new Promise(resolve => {
        this.drives.loading.enqueue(driveAddress, resolve)
      })
    }

    this.drives.loading.add(driveAddress)

    const drive = new Hyperdrive(this.store, driveAddress, {
      sparse: !this.isPataka,
      sparseMetadata: false
    })

    await new Promise((resolve, reject) => {
      if (drive.discoveryKey) resolve()
      else drive.on('ready', resolve)
    })
    await this._connect(drive)

    drive.on('update', () => log(`drive ${printKey(drive.key)} updated`))

    this.drives.remote.set(driveAddress, drive)
    this.drives.loading.resolveQueue(driveAddress, drive)
    // remove from "drives.loading" and resolve all queued _loadDrive

    return drive
  }

  // Connects to a peer through corestore networker.
  async _connect (drive) {
    log('Connecting to peers drive ', printKey(drive.key))
    // await new Promise((resolve, reject) => {
    //   this.networker.once('peer-add', resolve)
    //   this.networker.once('flushed', resolve)
    //   this.networker.configure(drive.discoveryKey, { announce: true, lookup: true })
    // })
    await this.networker.configure(drive.discoveryKey, { announce: true, lookup: true })
    log(`Connected to drive ${printKey(drive.key)} with ${drive.peers.length} peers`)
  }

  // Adds a file to own Hyperdrive
  async addFile (localFilePath, opts = {}) {
    if (this.isPataka) throw new Error('addFile not a valid pataka method')

    const fileName = opts.targetFileName || path.basename(localFilePath)
    // TODO add warning in case localFileName is already uploaded - give option to overwrite or choose new name
    const source = fs.createReadStream(localFilePath)

    return this.addFileStream(source, fileName, opts)
  }

  // Adds a file to own Hyperdrive by receiving a readstream
  async addFileStream (fileReadStream, fileName, opts = {}) {
    if (this.isPataka) throw new Error('addFileStream not a valid pataka method')

    // TODO: add warning in case localFileName is already uploaded - give option to overwrite or choose new name

    const fileEncryptionKey = opts.optionalFileEncryptionKey || ArtefactStore.artefactEncryptionKey()
    if (!isKey(fileEncryptionKey, ENCRYPTION_KEY_LENGTH)) throw new Error('Badly formatted encryption key')
    const target = this.createWriteStream(fileName, fileEncryptionKey)
    fileReadStream.pipe(target)
    await this.fileReady(fileName)

    return {
      driveAddress: toString(this.driveAddress),
      fileName,
      fileEncryptionKey: toString(fileEncryptionKey)
    }
  }

  // Prepares to add a file to user's own hyperdrive. Returns a writable stream.
  createWriteStream (artefactFileName, artefactEncryptionKey, opts) {
    if (this.isPataka) throw new Error('createWriteStream not a valid pataka method')

    if (!isKey(artefactEncryptionKey, ENCRYPTION_KEY_LENGTH)) throw new Error('Badly formatted encryption key')
    log('Adding file ', artefactFileName)
    const encryptStream = streamCipher(artefactEncryptionKey)
    const target = this.drives.personal
      .createWriteStream(artefactFileName, opts)
      .on('error', (err) => encryptStream.destroy(err))
    encryptStream.pipe(target)
    return encryptStream
  }

  // When writing to a hyperdrive, ensures the hyperdrive has updated and the file is accessible once written.
  async fileReady (artefactFileName) {
    if (this.pataka) return

    const self = this
    await new Promise((resolve, reject) => {
      self.drives.personal.stat(artefactFileName, (err) => {
        if (!err) return resolve()
        self.drives.personal.on('update', resolve)
      })
    })
    log(`File ${artefactFileName} added`)
  }

  // Prepares to read a file from a given hyperdrive in the corestore
  // opts // offset // {start: number} is the position in the file at which to start the readstream
  async getReadStream (artefactFileName, driveAddress, artefactEncryptionKey, opts = {}) {
    if (this.pataka) return

    if (!isKey(artefactEncryptionKey, ENCRYPTION_KEY_LENGTH)) throw new Error('Badly formatted encryption key')
    if (!isKey(driveAddress, DRIVE_ADDRESS_LENGTH)) throw new Error('Badly formatted drive address')
    artefactEncryptionKey = toBuffer(artefactEncryptionKey)

    const encryptStream = streamCipher(artefactEncryptionKey, opts.start)
    const drive = await this._getDrive(driveAddress)
    log(`Drive has ${drive.peers.length} peer(s)`)
    log('Flushed: ', this.networker.flushed(drive.discoveryKey))
    // log(drive.peers)
    log(`Trying to read file ${artefactFileName}`)
    drive.download(artefactFileName, () => {
      log(`${artefactFileName} downloaded`)
    })
    const source = drive
      .createReadStream(artefactFileName, opts)
      .on('error', (err) => { encryptStream.destroy(err) })
    source.pipe(encryptStream)
    return encryptStream
  }

  // Replicates one user's artefactStore to another's. Returns a stream.
  _replicate (isInitiator, opts) {
    return this.store.replicate(isInitiator, opts)
  }

  // Removes a file locally
  async remove (artefactFileName, driveAddress) {
    log(`Removing file ${driveAddress} : ${artefactFileName} from local storeage`)

    const drive = await this._getDrive(driveAddress)
    // await p(drive.clear)(artefactFileName) // needs this context D:
    return await new Promise((resolve, reject) => {
      drive.clear(artefactFileName, (err) => err ? reject(err) : resolve())
    })
  }

  // Given an artefactFileName and its driveAddress, returns the size of the file
  async fileSize (artefactFileName, driveAddress) {
    const stats = await this.fileStats(artefactFileName, driveAddress)
    return stats.size
  }

  async fileStats (artefactFileName, driveAddress) {
    const drive = await this._getDrive(driveAddress)

    return new Promise((resolve, reject) => {
      drive.stat(artefactFileName, (err, stats) => err ? reject(err) : resolve(stats))
    })
  }

  // Given a drive address returns size of locally stored drive
  async driveSize (driveAddress) {
    const drive = await this._getDrive(driveAddress)
    const feed = await new Promise((resolve, reject) => {
      drive.getContent((err, feed) => err ? reject(err) : resolve(feed))
    })

    if (typeof this.storagePath !== 'string') throw new Error('ArtefactStore#driveSize requires a storagePath that is a string')

    // fs.stat is an fs module method to give info about a file/system
    try {
      const stats = await stat(
        path.resolve(
          this.storagePath,
          feed.discoveryKey.slice(0, 1).toString('hex'),
          feed.discoveryKey.slice(1, 2).toString('hex'),
          feed.discoveryKey.toString('hex'),
          'data'
        )
        // where hyperdrive file structure stores the data
      )
      return stats.size
    } catch (err) {
      if (!err.message.match(/no such file or directory/)) console.warn(err)
      return 0
    }
  }

  async close () {
    log('Closing network connections')
    await this.networker.close()
    log('Network connections closed')
  }

  /* Getters */
  get discoveryKey () {
    return this.drives.personal && this.drives.personal.discoveryKey
  }

  get driveAddress () {
    return this.drives.personal && this.drives.personal.key
  }

  get remoteDrives () {
    return this.drives.remote.listDrives()
  }

  /* Static */
  static artefactEncryptionKey () {
    // generates a unique key for encrypting each artefact added to the artefactStore
    return streamCipher.key() // Buffer
  }
}

// function logEvents (emitter, name) {
//   const emit = emitter.emit
//   name = name ? `(${name}) ` : ''
//   emitter.emit = (...args) => {
//     // console.log(`\x1b[33m    ----${args[0]} ${inspect(args)}\x1b[0m`)
//     console.log(` ----- ${args[0]}`)
//     emit.apply(emitter, args)
//   }
// }

function DriveStore (normaliseKey = toString) {
  const store = new Map()

  return {
    set (driveAddress, drive) {
      return store.set(normaliseKey(driveAddress), drive)
    },
    get (driveAddress) {
      return store.get(normaliseKey(driveAddress))
    },
    has (driveAddress) {
      return store.has(normaliseKey(driveAddress))
    },
    listDrives () {
      return Array.from(store.values())
    }
  }
}

function DriveLoading (normaliseKey = toString) {
  const store = new Map()

  return {
    has (driveAddress) {
      return store.has(normaliseKey(driveAddress))
    },
    add (driveAddress) {
      store.set(normaliseKey(driveAddress), Obz())
    },
    enqueue (driveAddress, resolve) {
      if (typeof resolve !== 'function') throw new Error('DriveLoading#enqueue expected a function, got ', typeof resolve)
      const obz = store.get(normaliseKey(driveAddress))
      obz.once(resolve)
    },
    resolveQueue (driveAddress, drive) {
      const key = normaliseKey(driveAddress)
      const obz = store.get(key)
      if (!obz) return

      store.delete(key)

      obz.set(drive)
    }
  }
}
