const ArtefactStore = require('.')
const path = require('path')

async function main () {
  const alice = new ArtefactStore('./alice')
  await alice.ready()
  const inputFile = path.join(path.resolve(__dirname), './README.md')

  const { fileName, fileEncryptionKey } = await alice.addFile(inputFile)
  const readStream = await alice.getReadStream(fileName, alice.driveAddress, fileEncryptionKey)
  readStream.pipe(process.stdout)
  alice.close()
}

main()
